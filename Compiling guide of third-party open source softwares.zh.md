[EN](Compiling guide of third-party open source softwares.en.md)|CN

## 开源第三方软件编译或交叉编译指导

[TOC]



### FFmepg编译方法

1.下载x264(如果不需要使用ffmpeg进行H264的编码，可直接跳到ffmpeg的编译)：
	获取源码： git clone https://code.videolan.org/videolan/x264.git
2.进入x264软件包根目录，请注意修改命令中的路径为绝对路径，依次执行以下命令：
	```
	#./configure \
		--prefix=<where you want to install>/x264 \
		--enable-shared \
		--enable-static
	#make
	#make install
	```
3.下载ffmpeg，
	下载地址：https://github.com/FFmpeg/FFmpeg/releases
	推荐版本：4.1.3及以上版本
4.解压后，进入软件包的根目录，请注意修改命令中的路径为绝对路径，依次执行以下命令：
	```
	#./configure \
		--prefix=<whare you want to install>/ffmpeg \
		--target-os=linux \
		--enable-shared \
		--disable-doc 
	#make
	#make install
	```
	请注意，如果需要连接x264，configure时，添加以下内容:
	```
	--enable-libx264 --extra-cflags=-I<where you install>/x264/include --extra-ldflags=-L<whare you install>/x264/lib --enable-gpl
	```
5.编译链接ffmpeg
	Header file: <where you instll ffmpeg>/ffmpeg/include
	shared library folder: <where you install ffmpeg>/ffmpeg/lib
						   <where you install x264>/x264/lib
6.将ffmpeg和x264的链接库目录添加到系统动态链接库目录:
	```
	#export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:<where you install>/ffmpeg/lib:<where you install>/x264/lib
	```

### FFmepg交叉编译方法

1.安装Atlas500的交叉编译环境
	a.在ubuntu16.04(x86_64服务器), 解压Atlas500-ESP.zip，获得Euler_compile_env_cross.tar.gz；
	b.解压Euler_compile_env_cross.tar.gz，获取Euler_compile_env_cross文件夹，即为Atlas提供的交叉编译工具；
	c.修改/etc/profile，设置环境变量, 注意修改以下<where you place>为Euler_compile_env_cross文件夹的绝对路径，添加至文件最后:
	```
	export PATH=$PATH:<where you place>/Euler_compile_env_cross/arm/cross_compile/install/bin
	```
	应用生效
	```
	#source /etc/profile
	```
	d.查看交叉编译工具aarch64-linux-gnu-g++版本，验证是否生效
	```
	#aarch64-linux-gnu-g++ -v
	```
	PS：可参考《Atlsa500 应用软件开发指导书》，文档及软件包下载地址：
	https://support.huawei.com/enterprise/zh/ai-computing-platform/atlas-500-pid-23464104
2.下载x264(如果不需要使用ffmpeg进行H264的编码，可直接跳到ffmpeg的编译)：
	获取源码： git clone https://code.videolan.org/videolan/x264.git
3.进入x264软件包根目录，请注意修改命令中的路径为绝对路径，依次执行以下命令：
	```
	#./configure \
		--prefix=<where you want to install>/x264 \
		--host=aarch64-linux \
		--cross-prefix=aarch64-linux-gnu- \
		--enable-shared \
		--enable-static
	#make
	#make install
	```
4.下载ffmpeg，
	下载地址：https://github.com/FFmpeg/FFmpeg/releases
	推荐版本：4.1.3及以上版本
5.解压后，进入软件包的根目录，请注意修改命令中的路径为绝对路径，依次执行以下命令：
	```
	#./configure \
		--prefix=<whare you want to install>/ffmpeg \
		--target-os=linux \
		--arch=aarch64 \
		--enable-cross-compile \
		--cross-prefix=aarch64-linux-gnu- \
		--enable-shared \
		--disable-doc 
	#make
	#make install
	```
	请注意，如果需要连接x264，configure时，添加以下内容:
	```
	--enable-libx264 --extra-cflags=-I<where you install>/x264/include --extra-ldflags=-L<whare you install>/x264/lib --enable-gpl
	```
6.编译链接ffmpeg
	Header file: <where you instll ffmpeg>/ffmpeg/include
	shared library folder: <where you install ffmpeg>/ffmpeg/lib
						   <where you install x264>/x264/lib
7.将ffmpeg和x264的链接库拷贝到Atlas500上，并将该目录添加到系统动态链接库目录：
	```
	#export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:<where you place in Atlas500>/ffmpegsLibs/:<where you place in Atlas500>/x264Libs/
	```

### gflags交叉编译方法

1.安装Atlas500的交叉编译环境
	a.在ubuntu16.04(x86_64服务器), 解压Atlas500-ESP.zip，获得Euler_compile_env_cross.tar.gz；
	b.解压Euler_compile_env_cross.tar.gz，获取Euler_compile_env_cross文件夹，即为Atlas提供的交叉编译工具；
	c.修改/etc/profile，设置环境变量, 注意修改以下<where you place>为Euler_compile_env_cross文件夹的绝对路径，添加至文件最后:
	```
	export PATH=$PATH:<where you place>/Euler_compile_env_cross/arm/cross_compile/install/bin
	```
	应用生效
	```
	#source /etc/profile
	```
	d.查看交叉编译工具aarch64-linux-gnu-g++版本，验证是否生效
	```
	#aarch64-linux-gnu-g++ -v
	```
	e.编写交叉编译工具的cmake编译器脚本，并保存为Euler.cmake, 注意修改以下<where you place>为Euler_compile_env_cross文件夹的绝对路径:
	```
	set(CMAKE_SYSTEM_NAME Linux)
	set(CMAKE_SYSTEM_PROCESSOR aarch64)
	#
	set(tools <where you place>/Euler_compile_env_cross/arm/cross_compile/install)
	set(CMAKE_SYSROOT ${tools}/sysroot)
	set(CMAKE_C_COMPILER ${tools}/bin/aarch64-linux-gnu-gcc)
	set(CMAKE_CXX_COMPILER ${tools}/bin/aarch64-linux-gnu-g++)
	set(CMAKE_AR ${tools}/bin/aarch64-linux-gnu-ar)
	set(CMAKE_RANLIB ${tools}/bin/aarch64-linux-gnu-ranlib)
	set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
	set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
	set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
	set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
	```
	PS：可参考《Atlsa500 应用软件开发指导书》，文档及软件包下载地址：
	https://support.huawei.com/enterprise/zh/ai-computing-platform/atlas-500-pid-23464104
2.下载gflags源码
	源码地址： https://github.com/gflags/gflags/releases
	推荐版本： 2.2.2
3.解压后，进入gflags软件包根目录，请注意修改命令中的路径为绝对路径，依次执行以下命令：
	```
	#mkdir build
	#cd build
	#cmake \
		-DCMAKE_TOOLCHAIN_FILE=<where you place Euler.cmake>/Euler.cmake \
		-DCMAKE_INSTALL_PREFIX=<where you want to install>/gflags \
		-DBUILD_SHARED_LIBS=ON ..
	#make
	#make install
	```
4.链接gflags:
	Header file： <where you want to install>/gflags/include/
	shared library folder: <where you want to install>/gflags/lib/
5.将gflags的链接库拷贝到Atlas500上，并将该目录添加到系统动态链接目录：
	```
	#export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:<where you place in Atlas500>/gflagsLib/
	```

### Opencv交叉编译方法

1.安装Atlas500的交叉编译环境
	a.在ubuntu16.04(x86_64服务器), 解压Atlas500-ESP.zip，获得Euler_compile_env_cross.tar.gz；
	b.解压Euler_compile_env_cross.tar.gz，获取Euler_compile_env_cross文件夹，即为Atlas提供的交叉编译工具；
	c.修改/etc/profile，设置环境变量, 注意修改以下<where you place>为Euler_compile_env_cross文件夹的绝对路径，添加至文件最后:
	```
	export PATH=$PATH:<where you place>/Euler_compile_env_cross/arm/cross_compile/install/bin
	```
	应用生效
	```
	#source /etc/profile
	```
	d.查看交叉编译工具aarch64-linux-gnu-g++版本，验证是否生效
	```
	#aarch64-linux-gnu-g++ -v
	```
	e.,**请务必使用3.14及以上版本cmake**，编写交叉编译工具的cmake编译器脚本，并保存为Euler.cmake，注意修改以下<where you place>为Euler_compile_env_cross文件夹的绝对路径:
	```
	set(CMAKE_SYSTEM_NAME Linux)
	set(CMAKE_SYSTEM_PROCESSOR aarch64)
	#
	set(tools <where you place>/Euler_compile_env_cross/arm/cross_compile/install)
	set(CMAKE_SYSROOT ${tools}/sysroot)
	set(CMAKE_C_COMPILER ${tools}/bin/aarch64-linux-gnu-gcc)
	set(CMAKE_CXX_COMPILER ${tools}/bin/aarch64-linux-gnu-g++)
	set(CMAKE_AR ${tools}/bin/aarch64-linux-gnu-ar)
	set(CMAKE_RANLIB ${tools}/bin/aarch64-linux-gnu-ranlib)
	set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
	set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
	set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
	set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
	```
	PS：可参考《Atlsa500 应用软件开发指导书》，文档及软件包下载地址：
	https://support.huawei.com/enterprise/zh/ai-computing-platform/atlas-500-pid-23464104
2.下载opencv源码
	源码地址： https://github.com/opencv/opencv/releases
	推荐版本： 请根据Atlas软件版本选择合适版本
3.解压后，进入opencv软件包根目录，请注意修改命令中的路径为绝对路径，依次执行以下命令：
	```
	#mkdir build
	#cd build
	#cmake \
		-DCMAKE_TOOLCHAIN_FILE=<where you place Euler.cmake>/Euler.cmake \
		-DCMAKE_INSTALL_PREFIX=<where you want to install>/opencv \
		-DWITH_WEBP=OFF \ 
		-DBUILD_opencv_world=ON ..
	#make
	#make install
	```
4.编译时,链接opencv:
	Header file： <where you want to install>/opencv/include/
	Shared library folder: <where you want to install>/opencv/lib/
5.将opencv的链接库拷贝到Atlas500上，并将该目录添加到系统动态连接口目录：
	```
	#export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:<where you place in Atlas500>/opencvLibs
	```