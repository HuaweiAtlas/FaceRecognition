/**
 * ============================================================================
 *
 * Copyright (C) Huawei Technologies Co., Ltd. 2019. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1 Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   2 Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   3 Neither the names of the copyright holders nor the names of the
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * ============================================================================
 */
#ifndef ATLASFACEDEMO_RAWDATABUFFER_H
#define ATLASFACEDEMO_RAWDATABUFFER_H

#include "hiaiengine/data_type.h"
#include "hiaiengine/data_type_reg.h"
#include <memory>

typedef struct tagRawDataBufferHigh {
    std::shared_ptr<uint8_t> data;
    uint32_t len_of_byte;  // buffer size
    uint64_t frameId;
    uint32_t channelId;
    uint32_t format;
    uint32_t isDisplayChannel;
} RawDataBufferHigh;

inline void GetTransSearPtr(void* inputPtr, std::string& ctrlStr, uint8_t*& dataPtr, uint32_t& dataLen)
{
    RawDataBufferHigh* engineTrans = (RawDataBufferHigh*)inputPtr;
    ctrlStr  = std::string((char*)inputPtr, sizeof(RawDataBufferHigh));
    dataPtr = (uint8_t*)engineTrans->data.get();
    dataLen = engineTrans->len_of_byte;
}

inline std::shared_ptr<void> GetTransDearPtr(const char* ctrlPtr, const uint32_t& ctrlLen, const uint8_t* dataPtr, 
    const uint32_t& dataLen)
{
    RawDataBufferHigh* engineTrans = (RawDataBufferHigh*)ctrlPtr;
    std::shared_ptr<RawDataBufferHigh> engineTranPtr(new RawDataBufferHigh);
    engineTranPtr->frameId = engineTrans->frameId;
    engineTranPtr->channelId = engineTrans->channelId;
    engineTranPtr->format = engineTrans->format;
    engineTranPtr->isDisplayChannel = engineTrans->isDisplayChannel;
    engineTranPtr->len_of_byte = engineTrans->len_of_byte;
    engineTranPtr->data.reset(const_cast<uint8_t*>(dataPtr), hiai::Graph::ReleaseDataBuffer);
    return std::static_pointer_cast<void>(engineTranPtr);
}

#endif