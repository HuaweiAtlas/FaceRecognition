/**
 * ============================================================================
 *
 * Copyright (C) Huawei Technologies Co., Ltd. 2019. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1 Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   2 Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   3 Neither the names of the copyright holders nor the names of the
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * ============================================================================
 */
#include "StreamDataOutputEngine.h"
#include "engine_tools.h"
#include <chrono>
#include <iomanip>
#include <sstream>
#include <stdexcept>
#include <string>
#include <sys/stat.h>
#include <unistd.h>
#include "inc/common.h"

using std::shared_ptr;
using std::to_string;

const int IMAGE_WIDTH_1920 = 1920;
const int IMAGE_HEIGHT_1080 = 1080;
const int IMAGE_WIDTH_RESIZE_416 = 416;
const int IMAGE_HEIGHT_RESIZE_416 = 416;
const int MAX_PIXEL_255 = 255;
const int FRAME_READY_QUEUE_MAX = 500;
const int TIMES_SECOND_MICROSECOND = 1000000;
HIAI_StatusT StreamDataOutputEngine::ParamsCheck(const hiai::AIConfig& config, std::string& link)
{
    std::map<string, string> keyValueConfig = KvMap(config);
    if (CheckEmpty(keyValueConfig["rtsp_link"]) != HIAI_OK) {
        std::cout << "rtsp_link is empty, please check!" << std::endl;
        return HIAI_ERROR;
    }
    link = keyValueConfig["rtsp_link"];

    if (CheckEmpty(keyValueConfig["feature_lib_path"]) != HIAI_OK) {
        std::cout << "feature_lib_path is empty, please check!" << std::endl;
        return HIAI_ERROR;
    }
    std::string featureLibPath = keyValueConfig["feature_lib_path"];

    if (CheckEmpty(keyValueConfig["feature_len"]) != HIAI_OK) {
        std::cout << "feature_len is empty, please check!" << std::endl;
        return HIAI_ERROR;
    }
    uint32_t featureLen = 0;
    if (!(std::stringstream(keyValueConfig["feature_len"]) >> featureLen)) {
        std::cout << "feature_len is not digit,please check!" << std::endl;
        return HIAI_ERROR;
    }

    if (CheckEmpty(keyValueConfig["feature_num"]) != HIAI_OK) {
        std::cout << "feature_num is empty, please check!" << std::endl;
        return HIAI_ERROR;
    }
    uint32_t featureNum = 0;
    if (!(std::stringstream(keyValueConfig["feature_num"]) >> featureNum)) {
        std::cout << "feature_num is not digit,please check!" << std::endl;
        return HIAI_ERROR;
    }

    const int FEATURE_LEN = 512;
    if (featureLen != FEATURE_LEN || featureNum <= 0) {
        std::cout << "feature_len or feature_num in graph.config is invalid, please check!" << std::endl;
        return HIAI_ERROR;
    }

    if (FeaturelibRead(featureLib, featureLibPath, featureLen, featureNum) != 0) {
        return HIAI_ERROR;
    }
    return HIAI_OK;
}

HIAI_StatusT StreamDataOutputEngine::AvInit(std::string& rtspLink)
{
    // init to push frame
    avformat_network_init();
    pEncoder = avcodec_find_encoder(AV_CODEC_ID_MPEG4);
    if (pEncoder == NULL) {
        return HIAI_ERROR;
    }
    pCodecCtx = avcodec_alloc_context3(pEncoder);
    if (pCodecCtx == NULL) {
        return HIAI_ERROR;
    }
    pCodecCtx->codec_id = pEncoder->id;
    pCodecCtx->thread_count = 8;
    pCodecCtx->bit_rate = 1 * 1024 * 1024 * 4;
    pCodecCtx->width = IMAGE_WIDTH_1920;
    pCodecCtx->height = IMAGE_HEIGHT_1080;
    pCodecCtx->time_base.num = 1;
    pCodecCtx->time_base.den = 12;
    pCodecCtx->gop_size = 100;
    pCodecCtx->max_b_frames = 0;
    pCodecCtx->pix_fmt = AV_PIX_FMT_YUV420P;

    avcodec_open2(pCodecCtx, pEncoder, 0);
    avformat_alloc_output_context2(&pFormatCtx, NULL, "rtsp", rtspLink.c_str());
    if (pFormatCtx == NULL) {
        std::cout << "StreamDataOutputEngine allocate output context failed!" << std::endl;
        return HIAI_ERROR;
    }
    video_steam = avformat_new_stream(pFormatCtx, NULL);
    if (video_steam == NULL) {
        return HIAI_ERROR;
    }
    video_steam->codecpar->codec_tag = 0;
    avcodec_parameters_from_context(video_steam->codecpar, pCodecCtx);
    av_dump_format(pFormatCtx, 0, rtspLink.c_str(), 1);
    AVDictionary* format_opts = NULL;
    av_dict_set(&format_opts, "rtsp_transport", "tcp", 0);

    frame = av_frame_alloc();
    if (frame == NULL) {
        return HIAI_ERROR;
    }
    frame->format = AV_PIX_FMT_NV12;
    frame->width = IMAGE_WIDTH_1920;
    frame->height = IMAGE_HEIGHT_1080;
    frame->pts = 0;
    if (avformat_write_header(pFormatCtx, &format_opts) != 0) {
        std::cout << "StreamDataOutputEngine write header failed!" << std::endl;
        return HIAI_ERROR;
    }
    isHeaderWrite = true;

    int mss = memset_s(&packet, sizeof(packet), 0, sizeof(AVPacket));
    if (mss != 0){
        return HIAI_ERROR;
    }
    isPacketSet = true;

    readyFramYuv = new uint8_t[IMAGE_WIDTH_1920 * IMAGE_HEIGHT_1080 * 3 / 2];
    if (readyFramYuv == NULL) {
        return HIAI_ERROR;
    }

    framePushThreadRunner = std::thread(&StreamDataOutputEngine::FramePushThread, this);

    return HIAI_OK;
}

uint64_t StreamDataOutputEngine::GetCurentTimeStamp()
{
    gettimeofday(&currentTimeval, NULL);
    return currentTimeval.tv_sec * TIMES_SECOND_MICROSECOND + currentTimeval.tv_usec;
}

HIAI_StatusT StreamDataOutputEngine::Init(
    const hiai::AIConfig& config, const std::vector<hiai::AIModelDescription>& model_desc)
{
    std::string rtspLink;
    if (ParamsCheck(config, rtspLink) != HIAI_OK) {
        return HIAI_ERROR;
    }

    frameDataMap.resize(g_cameraNum);
    structuredInfoMap.resize(g_cameraNum);
    frameReadyQueue.resize(g_cameraNum);

    const int MAX_DISPLAY_NUM = 12;
    const int MIN_DISPLAY_NUM = 1;
    if (g_cameraNum < MIN_DISPLAY_NUM || g_cameraNum > MAX_DISPLAY_NUM) {
        std::cout <<"camera num must be in [1,12], current camera num: " << g_cameraNum << std::endl;
        return HIAI_ERROR;
    }
    if (g_cameraNum <= 1) {
        rowNum = 1;
        colNum = 1;
    } else if (g_cameraNum <= 2) {
        rowNum = 2;
        colNum = 1;     
    } else if (g_cameraNum <= 4) {
        rowNum = 2;
        colNum = 2; 
    } else if (g_cameraNum <= 6) {
        rowNum = 2;
        colNum = 3; 
    } else if (g_cameraNum <= 8) {
        rowNum = 2;
        colNum = 4; 
    } else if (g_cameraNum <= 12) {
        rowNum = 3;
        colNum = 4; 
    }

    subImageWidth = IMAGE_WIDTH_RESIZE_416;
    subImageHeight = IMAGE_HEIGHT_RESIZE_416;     

    displayRect.resize(rowNum * colNum);
    displayMat = cv::Mat::zeros(subImageHeight * rowNum, subImageWidth * colNum, CV_8UC3);
    for (int i = 0; i < g_cameraNum; i++) {
        int c = i % colNum;
        int r = i / colNum;
        displayRect[i] = cv::Rect(c * subImageWidth, r * subImageHeight, subImageWidth, subImageHeight);
    }

    if (g_isDisplay == 1) {
        if (HIAI_OK != AvInit(rtspLink)) {
            return HIAI_ERROR;
        }
    }

    initialTimeStamp = GetCurentTimeStamp();

    std::cout << "StreamDataOutputEngine init SUCCESS!" << std::endl;
    return HIAI_OK;
}


StreamDataOutputEngine::~StreamDataOutputEngine()
{
    condVar.notify_all();
    if (framePushThreadRunner.joinable()) {
        framePushThreadRunner.join();
    }

    if (pFormatCtx && isHeaderWrite) {
        if (av_write_trailer(pFormatCtx) != 0) {
            std::cout << "StreamDataOutputEngine failed to write tailer!" << std::endl;
        }
    }
	
    if (pEncoder) {
        pEncoder = NULL;
    }

    if (video_steam) {
        avcodec_close(pCodecCtx);
        video_steam = NULL;
    }

    if (pFormatCtx) {
        avio_closep(&pFormatCtx->pb);
        avformat_free_context(pFormatCtx);
        pFormatCtx = NULL;
    }

    if (pCodecCtx) {
        avcodec_free_context(&pCodecCtx);
    }

    if (frame) {
        av_frame_free(&frame);
    }

    if (isPacketSet) {
        av_packet_unref(&packet);
    }

    if (readyFramYuv) {
        delete[] readyFramYuv;
        readyFramYuv = NULL;
    }
}

void StreamDataOutputEngine::FramePushThread()
{
    while ((!g_isResume) && (!g_signalRecieved)) {
        unique_lock<mutex> locker(framePushDequeMutex);
        while (framePushQueue.empty()) {
            condVar.wait(locker);
            if ((g_isResume) || (g_signalRecieved)) {
                locker.unlock();
                return;
            }
        }
        std::shared_ptr<uint8_t> framePushCache = framePushQueue.front();
        framePushQueue.pop_front();
        locker.unlock();

        uint8_t* buff = framePushCache.get();
        int size = IMAGE_WIDTH_1920 * IMAGE_HEIGHT_1080;
        frame->pts = iFrame;
        iFrame++;
        frame->data[0] = buff;
        frame->data[1] = buff + size;
        frame->data[2] = buff + size * 5 / 4;
        frame->linesize[0] = IMAGE_WIDTH_1920;
        frame->linesize[1] = IMAGE_WIDTH_1920 / 2;
        frame->linesize[2] = IMAGE_WIDTH_1920 / 2;

        int ret = avcodec_send_frame(pCodecCtx, frame);
        ret = avcodec_receive_packet(pCodecCtx, &packet);
        if (ret != 0 || packet.size > 0) {
            ;
        } else {
            continue;
        }
        packet.pts = av_rescale_q(packet.pts, pCodecCtx->time_base, video_steam->time_base);
        packet.dts = av_rescale_q(packet.dts, pCodecCtx->time_base, video_steam->time_base);
        packet.duration = av_rescale_q(packet.duration, pCodecCtx->time_base, video_steam->time_base);
        av_interleaved_write_frame(pFormatCtx, &packet);
         
    }
}

HIAI_StatusT StreamDataOutputEngine::PrepareToPush(std::shared_ptr<DeviceStreamData> imageInput)
{
    if (frameReadyQueue[imageInput->info.channelId].size() > FRAME_READY_QUEUE_MAX) {
        return HIAI_OK;
    }

    frameReadyQueue[imageInput->info.channelId].push_back(imageInput);
    
    for (int i = 0; i < g_cameraNum; i++) {
        if (frameReadyQueue[i].empty()) {
            return HIAI_OK;
        }
    }
    
    float throughoutput = iFrame / (1.0 * (GetCurentTimeStamp() - initialTimeStamp) / TIMES_SECOND_MICROSECOND);

    for (int k = 0; k < g_cameraNum; k++) {
        std::shared_ptr<DeviceStreamData> topImageInput = frameReadyQueue[k].front();
        int height = IMAGE_HEIGHT_RESIZE_416;
        int width = IMAGE_WIDTH_RESIZE_416;
        cv::Mat srcNv12Mat(height * 3 / 2, width, CV_8UC1, topImageInput->imgOrigin.buf.data.get());
        cv::Mat dstRGB888 = displayMat(displayRect[topImageInput->info.channelId]);
        cv::cvtColor(srcNv12Mat, dstRGB888, cv::COLOR_YUV2BGR_NV12);
        CvFont font;
        cvInitFont(&font, CV_FONT_HERSHEY_PLAIN, 1.5f, 1.5f, 0, 2, CV_AA);
        CvPoint leftTopPoint, rightBottomPoint;
        for (int i = 0; i < topImageInput->face.size(); i++) {

            int32_t featureNum = topImageInput->face[i].featureVector.len_of_byte / sizeof(float);
            float* featureVectorPtr = (float*)topImageInput->face[i].featureVector.data.get();
            float similarity = 0;

            int32_t objectId = SearchFeatureLib(featureLib, featureVectorPtr, featureNum, similarity);

            std::stringstream stream;
            stream << std::fixed << std::setprecision(3) << similarity;

            std::string strID;
            if (objectId < 0) {
                strID = stream.str() + ",ID=Unknown";
            } else {
                strID = stream.str() + ",ID=" + std::to_string(objectId);
            }
            leftTopPoint.x = topImageInput->face[i].info.location.anchor_lt.x * 1.0 / 
                            topImageInput->imgOrigin.widthAligned * IMAGE_WIDTH_RESIZE_416;
            leftTopPoint.y = topImageInput->face[i].info.location.anchor_lt.y * 1.0 / 
                            topImageInput->imgOrigin.heightAligned * IMAGE_HEIGHT_RESIZE_416;
            rightBottomPoint.x = topImageInput->face[i].info.location.anchor_rb.x * 1.0 / 
                            topImageInput->imgOrigin.widthAligned * IMAGE_WIDTH_RESIZE_416;
            rightBottomPoint.y = topImageInput->face[i].info.location.anchor_rb.y * 1.0 / 
                            topImageInput->imgOrigin.heightAligned * IMAGE_HEIGHT_RESIZE_416;

            printf("channel: %u, frame id: #%u, bound box: (%d, %d),(%d, %d), largest "
               "similarity:%s, isTracked=%d, throughoutput=%.1f\n",
                topImageInput->info.channelId,
                (uint32_t)topImageInput->info.frameId,
                topImageInput->face[i].info.location.anchor_lt.x,
                topImageInput->face[i].info.location.anchor_lt.y,
                topImageInput->face[i].info.location.anchor_rb.x,
                topImageInput->face[i].info.location.anchor_rb.y,
                strID.c_str(),
                topImageInput->isTracked, throughoutput);
    
            cv::rectangle(dstRGB888, leftTopPoint, rightBottomPoint, cv::Scalar(0, 0, MAX_PIXEL_255), 1);
            cv::putText(dstRGB888,
                strID,
                cvPoint(leftTopPoint.x, leftTopPoint.y - 10),
                CV_FONT_HERSHEY_PLAIN,
                1,
                cv::Scalar(0, 0, MAX_PIXEL_255),
                1);
        }

        frameReadyQueue[k].pop_front();
    }

    if (g_isDisplay != 1) {
        return HIAI_OK;
    }

    cv::Mat displayMat1080p(IMAGE_HEIGHT_1080, IMAGE_WIDTH_1920, CV_8UC3);
    cv::resize(displayMat, displayMat1080p, cv::Size(IMAGE_WIDTH_1920, IMAGE_HEIGHT_1080), (0, 0), (0, 0), cv::INTER_LINEAR);
    uint32_t yuvHeight = IMAGE_HEIGHT_1080 * 3 / 2;

    std::shared_ptr<uint8_t> dstBuffer(new uint8_t[yuvHeight * IMAGE_WIDTH_1920]);
    cv::Mat dstImage(yuvHeight, IMAGE_WIDTH_1920, CV_8UC1, dstBuffer.get());
    cv::cvtColor(displayMat1080p, dstImage, cv::COLOR_BGR2YUV_I420);

    // FramePusher(dstImage.data);
    unique_lock<mutex> locker(framePushDequeMutex);
    framePushQueue.push_back(dstBuffer);
    locker.unlock();
    condVar.notify_one();
    return HIAI_OK;
}

HIAI_IMPL_ENGINE_PROCESS("StreamDataOutputEngine", StreamDataOutputEngine, SDO_INPUT_SIZE)
{
    if (arg1 != nullptr) {
        std::shared_ptr<RawDataBufferHigh> dataInput = static_pointer_cast<RawDataBufferHigh>(arg1);
        int channelID = dataInput->channelId;
        frameDataMap[channelID][dataInput->frameId] = dataInput;
        if (structuredInfoMap[channelID].find(dataInput->frameId) != structuredInfoMap[channelID].end()) {
            structuredInfoMap[channelID][dataInput->frameId]->imgOrigin.buf.data = dataInput->data;
            structuredInfoMap[channelID][dataInput->frameId]->imgOrigin.buf.len_of_byte = dataInput->len_of_byte;
            PrepareToPush(structuredInfoMap[channelID][dataInput->frameId]);
        }

        // erase the unused structured info
        auto iter = structuredInfoMap[channelID].begin();
        while (iter != structuredInfoMap[channelID].end()) {
            if ((iter->first) < dataInput->frameId) {
                iter = structuredInfoMap[channelID].erase(iter);
            } else {
                iter++;
            }
        }
    }

    if (arg0 != nullptr) {
        std::shared_ptr<DeviceStreamData> dataInput = static_pointer_cast<DeviceStreamData>(arg0);

        int channelID = dataInput->info.channelId;
        // the iuput data is structured info
        if (structuredInfoMap[channelID].find(dataInput->info.frameId) != structuredInfoMap[channelID].end()) {
            structuredInfoMap[channelID][dataInput->info.frameId]->face.insert(
                structuredInfoMap[channelID][dataInput->info.frameId]->face.end(),
                dataInput->face.begin(),
                dataInput->face.end());
        } else {
            structuredInfoMap[channelID][dataInput->info.frameId] = dataInput;
        }

        if (frameDataMap[channelID].find(dataInput->info.frameId) != frameDataMap[channelID].end()) {
            dataInput->imgOrigin.buf.data = frameDataMap[channelID][dataInput->info.frameId]->data;
            dataInput->imgOrigin.buf.len_of_byte = frameDataMap[channelID][dataInput->info.frameId]->len_of_byte;
            PrepareToPush(dataInput);
        }

        // erase the unused frame data
        auto iter = frameDataMap[channelID].begin();
        while (iter != frameDataMap[channelID].end()) {
            if ((iter->first) < dataInput->info.frameId) {
                iter = frameDataMap[channelID].erase(iter);
            } else {
                iter++;
            }
        }
    }
    return HIAI_OK;
}