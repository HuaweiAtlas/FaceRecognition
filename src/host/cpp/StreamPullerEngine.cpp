/**
 * ============================================================================
 *
 * Copyright (C) Huawei Technologies Co., Ltd. 2019. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1 Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   2 Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   3 Neither the names of the copyright holders nor the names of the
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * ============================================================================
 */

#include "StreamPullerEngine.h"
#include "engine_tools.h"
#include "hiaiengine/ai_memory.h"
#include <chrono>
#include "inc/common.h"

const int FAILURE_NUM = 5;
const int SLEEP_TIME = 30;
const int SEND_SLEEP_TIME = 10;
const int MAX_QUEUE_NUM = 500;

shared_ptr<AVFormatContext> StreamPullerEngine::CreateFormatContext(string streamName)
{
    AVFormatContext* formatContext = nullptr;
    AVDictionary* options = nullptr;
    av_dict_set(&options, "rtsp_transport", "tcp", 0);
    av_dict_set(&options, "stimeout", "3000000", 0);
    int ret = avformat_open_input(&formatContext, streamName.c_str(), nullptr, &options);
    if (options != nullptr) {
        av_dict_free(&options);
    }
    if (ret != 0) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "Open input stream FAILED %s, ret=%d.", streamName.c_str(), ret);
        return nullptr;
    }
    ret = avformat_find_stream_info(formatContext, nullptr);
    if (ret != 0) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "Don't find stream information.");
        return nullptr;
    }
    return shared_ptr<AVFormatContext>(formatContext, [](AVFormatContext* p) {
        if (p) {
            avformat_close_input(&p);
        }
    });
}

int StreamPullerEngine::GetStreamInfo(shared_ptr<AVFormatContext> pFormatCtx, int& format, int& videoIndex)
{
    if (pFormatCtx == nullptr) {
        cout << "pFormatCtx is null!." << endl;
        return -1;
    }

    videoIndex = -1;
    for (int i = 0; i < pFormatCtx->nb_streams; i++) {
        AVStream* inStream = pFormatCtx->streams[i];
        if (inStream->codecpar->codec_type != AVMEDIA_TYPE_VIDEO) {
            cout << "Error codec_type " << inStream->codecpar->codec_type << endl;
            return -1;
        }
        videoIndex = i;
        AVCodecID codecId = inStream->codecpar->codec_id;
        if (codecId == AV_CODEC_ID_H264) {
            format = 0;
        } else if (codecId == AV_CODEC_ID_H265) {
            format = 1;
        } else {
            cout << "Error unsupported format " << codecId << endl;
            return -1;
        }
    }

    if (videoIndex == -1) {
        cout << "Didn't find a video stream." << endl;
        return -1;
    }
    return 0;
}

void StreamPullerEngine::TransferData()
{

    while (this->stop == 0 && !g_signalRecieved) {
        mtx.lock();
        if (dataQueues.empty()) {
            mtx.unlock();
            this_thread::sleep_for(chrono::milliseconds(SEND_SLEEP_TIME));
            continue;
        }
        shared_ptr<PacketInfo> sendPacket = dataQueues.front();
        dataQueues.pop();
        mtx.unlock();

        shared_ptr<RawDataBufferHigh> output = make_shared<RawDataBufferHigh>();
        output->channelId = this->channelId;
        output->format = this->format;
        output->isDisplayChannel = 1;
        output->data = sendPacket->buf;
        output->len_of_byte = sendPacket->len;
        output->frameId = sendPacket->frameNum;

        int ret;
        do {
            ret = hiai::Engine::SendData(0, "RawDataBufferHigh", static_pointer_cast<void>(output), 1000);
            if (ret == HIAI_QUEUE_FULL) {
                this_thread::sleep_for(chrono::milliseconds(SEND_SLEEP_TIME));
            }

            if (ret != HIAI_OK) {
				HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "channel:%d. StreamPuller send data failed:%d.", channelId, ret);
                cout << "channel " << channelId << " send data failed. sleep for 1 second." << endl;
                this_thread::sleep_for(chrono::seconds(1));
            }
        } while (ret == HIAI_QUEUE_FULL);  // Wait for sending data, until trans_queue is not full.
    }
}

void StreamPullerEngine::PullStreamDataLoop()
{
    int pullFailedCount = 0;
    uint64_t frameNum = 0;
    shared_ptr<AVFormatContext> pFormatCtx;
    while (pullFailedCount < FAILURE_NUM && this->stop == 0 && !g_signalRecieved) {
        pFormatCtx = CreateFormatContext(this->streamAddr);
        if (pFormatCtx == nullptr) {
			cout << "channel " << channelId << " create context failed." << endl;
            pullFailedCount++;
            this_thread::sleep_for(chrono::milliseconds(SLEEP_TIME));
            continue;
        }
        // get stream infomation
        int videoIndex = -1;
        if (GetStreamInfo(pFormatCtx, this->format, videoIndex) == -1) {
            pullFailedCount++;
            continue;
        }
        ReadFrame(pFormatCtx, frameNum, videoIndex);
        pullFailedCount++;
    }

    cout << "channel " << channelId << " PullStreamDataLoop end of stream." << endl;
    this->stop = 1;
    g_isResume = true;
}

int StreamPullerEngine::ReadFrame(shared_ptr<AVFormatContext> pFormatCtx, uint64_t& frameNum, int videoIndex)
{
    AVPacket pkt;
    int readFailedCount = 0;
    while (readFailedCount < FAILURE_NUM && this->stop == 0 && !g_signalRecieved) {
        av_init_packet(&pkt);
        int ret = av_read_frame(pFormatCtx.get(), &pkt);
        if (ret != 0) {
            readFailedCount++;
            cout << " Read frame failed. ErrCode: " << ret << endl;
            this_thread::sleep_for(chrono::milliseconds(1));
            continue;
        } else if (pkt.stream_index == videoIndex) {
            if (pkt.size <= 0) {
                readFailedCount++;
                cout << "Invalid pkt.size= " << pkt.size << endl;
                continue;
            }

            uint8_t* buffer = nullptr;
            HIAI_StatusT ret = hiai::HIAIMemory::HIAI_DMalloc(pkt.size, (void*&)buffer);
            if (ret != HIAI_OK || buffer == nullptr) {
                cout << "channel: " << this->channelId << " Dmalloc memory faild." << endl;
                av_packet_unref(&pkt);
                continue;
            }
            ret = memcpy_s(buffer, pkt.size, pkt.data, pkt.size);
            if (ret != 0) {
                cout << "channel: " << this->channelId << " memcpy faild." << endl;
                av_packet_unref(&pkt);
                hiai::HIAIMemory::HIAI_DFree(buffer);
                continue;
            }
            shared_ptr<PacketInfo> packetInfo = make_shared<PacketInfo>();
            packetInfo->len = pkt.size;
            packetInfo->frameNum = ++frameNum;
            mtx.lock();
            if (dataQueues.size() < MAX_QUEUE_NUM) {
                packetInfo->buf.reset(buffer, [](uint8_t* p) {});
                dataQueues.push(packetInfo);
            } else {
                packetInfo->buf.reset(buffer, [](uint8_t* p) {hiai::HIAIMemory::HIAI_DFree(p);});
            }
            mtx.unlock();
        }
        av_packet_unref(&pkt);
    }
    return -1;
}

StreamPullerEngine::~StreamPullerEngine()
{
    this->stop = 1;
    avformat_network_deinit();
    if (pullStreamThread.joinable()) {
        pullStreamThread.join();
    }

    if (sendDataThread.joinable()) {
        sendDataThread.join();
    }
}

void StreamPullerEngine::StartStream()
{
    pullStreamThread = thread(&StreamPullerEngine::PullStreamDataLoop, this);
    sendDataThread = thread(&StreamPullerEngine::TransferData, this);
    return;
}

HIAI_StatusT StreamPullerEngine::Init(const hiai::AIConfig& config, const vector<hiai::AIModelDescription>& model_desc)
{
    auto aimap = KvMap(config);

    if (!(stringstream(aimap["channel_id"]) >> channelId)) {
        cout << "channel_id is not digit,please check!" << endl;
        return HIAI_ERROR;
    }
    if (channelId < 0 || channelId >= g_NUM_CAMERAS) {
        cout << "channelId must be larger than or equal than 0 and less than " << g_NUM_CAMERAS << endl;
        return HIAI_ERROR;
    }

    avformat_network_init();
    return HIAI_OK;
}

HIAI_IMPL_ENGINE_PROCESS("StreamPullerEngine", StreamPullerEngine, RP_INPUT_SIZE)
{
    if (arg0 != nullptr) {
        shared_ptr<string> inputArg = static_pointer_cast<string>(arg0);
        if (!inputArg->empty()) {
            this->streamAddr = *inputArg;
            StartStream();
        }
    }
    return HIAI_OK;
}