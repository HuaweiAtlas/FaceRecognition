EN|[CN](Compiling guide of third-party open source softwares.zh.md)

## Compiling guide of third-party open source softwares

[TOC]



### FFmepg compiling method

1.Download x264 code(If you don't need to use ffmpeg for H264 encoding, you can skip to ffmpeg compile directly)：
	Get the source code： git clone https://code.videolan.org/videolan/x264.git
2.Enter the root directory of x264 source code，exceute the following commands in order：
	Please replace the path <where you want to install> with an real absolute path
	```
	#./configure \
		--prefix=<where you want to install>/x264 \
		--enable-shared \
		--enable-static
	#make
	#make install
	```
3.Download ffmpeg code
	url：https://github.com/FFmpeg/FFmpeg/releases
	Recommand： 4.1.3 and above
4.After unpack，enter the root directory of ffmpeg, exceute the following commands in order：
	Please replace the path <whare you want to install> with an real absolute path
	```
	#./configure \
		--prefix=<whare you want to install>/ffmpeg \
		--target-os=linux \
		--enable-shared \
		--disable-doc 
	#make
	#make install
	```
	Note: If need to use ffmpet to encode H264, add the following option in command ./configure:
	```
	--enable-libx264 --extra-cflags=-I<where you install>/x264/include --extra-ldflags=-L<whare you install>/x264/lib --enable-gpl
	```
5.Link ffmpeg in compile:
	Header file: <where you instll ffmpeg>/ffmpeg/include
	Shared library folder: <where you install ffmpeg>/ffmpeg/lib
						   <where you install x264>/x264/lib
6.Add the directory to the system dynamic link library directory:
	```
	#export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:<where you place>/ffmpeg/lib:<where you place>/x264/lib
	```

### FFmpeg cross compiling method

1.Install the Atlas500 cross compilation environment:
	a.On ubuntu16.04(x86_64 server), unpack Atlas500-ESP.zip，get Euler_compile_env_cross.tar.gz；
	b.Unpack uler_compile_env_cross.tar.gz，get Euler_compile_env_cross folder，which is cross compilation tool for Atlas500；
	c.Modify /etc/profile to set environment variables, add the command to the file /etc/profile:
	Please replace the path <where you place> in command with the real absolute path of the folder Euler_compile_env_cross:
	```
	export PATH=$PATH:<where you place>/Euler_compile_env_cross/arm/cross_compile/install/bin
	```
	Take effect:
	```
	#source /etc/profile
	```
	d. Check the aarch64-linux-gnu-g++ version of the cross compilation tool:
	```
	#aarch64-linux-gnu-g++ -v
	```
	Note： Refer to 《Atlas 500 Software Development Guide》 to install the Atlas500 cross compilation environment. The documents and software download address is as follows:
	https://support.huawei.com/enterprise/zh/ai-computing-platform/atlas-500-pid-23464104
2.Download x264 code(If you don't need to use ffmpeg for H264 encoding, you can skip to ffmpeg compile directly)：
	Get the source code： git clone https://code.videolan.org/videolan/x264.git
3.Enter the root directory of x264 source code，exceute the following commands in order：
	Please replace the path <where you want to install> with an real absolute path
	```
	#./configure \
		--prefix=<where you want to install>/x264 \
		--host=aarch64-linux \
		--cross-prefix=aarch64-linux-gnu- \
		--enable-shared \
		--enable-static
	#make
	#make install
	```
4.Download ffmpeg code，
	url：https://github.com/FFmpeg/FFmpeg/releases
	Recommand： 4.1.3 and above
5.After unpack，enter the root directory of ffmpeg, exceute the following commands in order：
	Please replace the path <whare you want to install> with an real absolute path：
	```
	#./configure \
		--prefix=<whare you want to install>/ffmpeg \
		--target-os=linux \
		--arch=aarch64 \
		--enable-cross-compile \
		--cross-prefix=aarch64-linux-gnu- \
		--enable-shared \
		--disable-doc 
	#make
	#make install
	```
	Note: If need to use ffmpet to encode H264, add the following option in command ./configure:
	```
	--enable-libx264 --extra-cflags=-I<where you install>/x264/include --extra-ldflags=-L<whare you install>/x264/lib --enable-gpl
	```
6.Link ffmpeg in compile
	Header file: <where you instll ffmpeg>/ffmpeg/include
	Shared library folder: <where you install ffmpeg>/ffmpeg/lib
						   <where you install x264>/x264/lib
7.Upload the shared library of fmpeg and x264 to Atlas500，add the directory to the system dynamic link library directory:
	```
	#export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:<where you place in Atlas500>/ffmpegsLibs/:<where you place in Atlas500>/x264Libs/```
	

### gflags cross compiling method

1.Install the Atlas500 cross compilation environment:
	a.On ubuntu16.04(x86_64 server), unpack Atlas500-ESP.zip，get Euler_compile_env_cross.tar.gz；
	b.Unpack uler_compile_env_cross.tar.gz，get Euler_compile_env_cross folder，which is cross compilation tool for Atlas500；
	c.Modify /etc/profile to set environment variables, add the command to the file /etc/profile:
	Please replace the path <where you place> in command with the real absolute path of the folder Euler_compile_env_cross:
	```
	export PATH=$PATH:<where you place>/Euler_compile_env_cross/arm/cross_compile/install/bin
	```
	Take effect:
	```
	#source /etc/profile
	```
	d. Check the aarch64-linux-gnu-g++ version of the cross compilation tool:
	```
	#aarch64-linux-gnu-g++ -v
	```
	e.Write the cmake script and save it as Euler.cmake:
	Please replace the path <where you place> in command with the real absolute path of the folder Euler_compile_env_cross:
	```
	set(CMAKE_SYSTEM_NAME Linux)
	set(CMAKE_SYSTEM_PROCESSOR aarch64)
	#
	set(tools <where you place>/Euler_compile_env_cross/arm/cross_compile/install)
	set(CMAKE_SYSROOT ${tools}/sysroot)
	set(CMAKE_C_COMPILER ${tools}/bin/aarch64-linux-gnu-gcc)
	set(CMAKE_CXX_COMPILER ${tools}/bin/aarch64-linux-gnu-g++)
	set(CMAKE_AR ${tools}/bin/aarch64-linux-gnu-ar)
	set(CMAKE_RANLIB ${tools}/bin/aarch64-linux-gnu-ranlib)
	set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
	set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
	set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
	set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
	```
	Note： Refer to 《Atlas 500 Software Development Guide》 to install the Atlas500 cross compilation environment. The documents and software download address is as follows:
	https://support.huawei.com/enterprise/zh/ai-computing-platform/atlas-500-pid-23464104
2.Download gflags code
	url： https://github.com/gflags/gflags/releases
	Recommand： 2.2.2
3.After unpack，enter the root directory of gflags, exceute the following commands in order：
	Please replace the path <where you place Euler.cmake> and <whare you want to install> with an real absolute path：
	```
	#mkdir build
	#cd build
	#cmake \
		-DCMAKE_TOOLCHAIN_FILE=<where you place Euler.cmake>/Euler.cmake \
		-DCMAKE_INSTALL_PREFIX=<where you want to install>/gflags \
		-DBUILD_SHARED_LIBS=ON ..
	#make
	#make install
	```
4.Link gflags in compile:
	Header file： <where you want to install>/gflags/include/
	Shared library folder: <where you want to install>/gflags/lib/
5.Upload the shared library of gflags to Atlas500，add the directory to the system dynamic link library directory:
	```
	#export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:<where you place in Atlas500>/gflagsLib/
	```

### opencv cross compiling method

1.Install the Atlas500 cross compilation environment:
	a.On ubuntu16.04(x86_64 server), unpack Atlas500-ESP.zip，get Euler_compile_env_cross.tar.gz；
	b.Unpack uler_compile_env_cross.tar.gz，get Euler_compile_env_cross folder，which is cross compilation tool for Atlas500；
	c.Modify /etc/profile to set environment variables, add the command to the file /etc/profile:
	Please replace the path <where you place> in command with the real absolute path of the folder Euler_compile_env_cross:
	```
	export PATH=$PATH:<where you place>/Euler_compile_env_cross/arm/cross_compile/install/bin
	```
	Take effect:
	```
	#source /etc/profile
	```
	d. Check the aarch64-linux-gnu-g++ version of the cross compilation tool:
	```
	#aarch64-linux-gnu-g++ -v
	```
	e. **Please use the cmake with 3.14 or higher version** to write the cmake script and save it as Euler.cmake:
	Replace the path <where you place> in command with the real absolute path of the folder Euler_compile_env_cross:
	```
	set(CMAKE_SYSTEM_NAME Linux)
	set(CMAKE_SYSTEM_PROCESSOR aarch64)
	#
	set(tools <where you place>/Euler_compile_env_cross/arm/cross_compile/install)
	set(CMAKE_SYSROOT ${tools}/sysroot)
	set(CMAKE_C_COMPILER ${tools}/bin/aarch64-linux-gnu-gcc)
	set(CMAKE_CXX_COMPILER ${tools}/bin/aarch64-linux-gnu-g++)
	set(CMAKE_AR ${tools}/bin/aarch64-linux-gnu-ar)
	set(CMAKE_RANLIB ${tools}/bin/aarch64-linux-gnu-ranlib)
	set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
	set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
	set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
	set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
	```
	Note： Refer to 《Atlas 500 Software Development Guide》 to install the Atlas500 cross compilation environment. The documents and software download address is as follows:
	https://support.huawei.com/enterprise/zh/ai-computing-platform/atlas-500-pid-23464104
2.Download opencv code
	url: https://github.com/opencv/opencv/releases
	Recommand: Please select the appropriate version according to the Atlas software version
3.After unpack，enter the root directory of opencv, exceute the following commands in order：
	Please replace the path <where you place Euler.cmake> and <whare you want to install> with an real absolute path：
	```
	#mkdir build
	#cd build
	#cmake \
		-DCMAKE_TOOLCHAIN_FILE=<where you place Euler.cmake>/Euler.cmake \
		-DCMAKE_INSTALL_PREFIX=<where you want to install>/opencv \
		-DWITH_WEBP=OFF \ 
		-DBUILD_opencv_world=ON ..
	#make
	#make install
	```
4.Link opencv in compile:
	Header file： <where you want to install>/opencv/include/
	Shared library folder: <where you want to install>/opencv/lib/
5.Upload the shared library of opencv to Atlas500，add the directory to the system dynamic link library directory:
	```
	#export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:<where you place in Atlas500>/opencvLibs
	```